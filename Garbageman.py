import os, glob,time

max_age = 60*60*24*7

print 'removing files older than %s seconds (1 week)' % max_age

for fname in glob.glob('/mnt/ssddisk/conviva/*'):
    stat = os.stat(fname)
    age = int(time.time()-stat.st_mtime )
    if age > max_age:
        os.remove(fname)
        print 'removed %s old %s' % (age, fname)
print 'finished'
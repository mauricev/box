# Import two classes from the boxsdk module - Client and OAuth2
from boxsdk import Client, OAuth2
from boxsdk.network.default_network import DefaultNetwork
from pprint import pformat
import json, sys, time, gzip, os, glob, shutil, operator

CLIENT_ID = 'a30jvs757519qsnimmkfs52s23cqsodf'
CLIENT_SECRET = '3ExzQa37PYAQWPYg9jWEW7PtQKuE3Edh'
AUTH_FILE = '/root/box/auth.json'


class LoggingNetwork(DefaultNetwork):
	def request(self, method, url, access_token, **kwargs):
		""" Base class override. Pretty-prints outgoing requests and incoming responses. """
		print '\x1b[36m{} {} {}\x1b[0m'.format(method, url, pformat(kwargs))
		response = super(LoggingNetwork, self).request(
			method, url, access_token, **kwargs
		)
		if response.ok:
			print '\x1b[32m{}\x1b[0m'.format(response.content)
		else:
			print '\x1b[31m{}\n{}\n{}\x1b[0m'.format(
				response.status_code,
				response.headers,
				pformat(response.content),
			)
		return response

def get_authentication_url():
	""" Used to initiate the exchange by which you get the authorization url """
	oauth = OAuth2(
		client_id=CLIENT_ID,
		client_secret=CLIENT_SECRET,
		)
	auth_url, csrf_token = oauth.get_authorization_url('https://entreprenerd.nl/box/authenticate')
	print 'go to URL: %s \n The callback will contain a parameter \'code\'. boot this script with the parameter to use it for getting the proper credentials' % auth_url
	sys.exit()

def get_initial_tokens(AUTH_CODE):
	""" First time exchange of the authenticationcode for the access and refresh tokens. Keeping them alive will be handled bij the SDK """
	oauth = OAuth2(
		client_id=CLIENT_ID,
		client_secret=CLIENT_SECRET,
		store_tokens=store_tokens,
		)
	ACCESS_TOKEN, REFRESH_TOKEN = oauth.authenticate(AUTH_CODE)


def store_tokens(ACCESS_TOKEN=None, REFRESH_TOKEN=None):
	""" Store the access and refresh tokens in an json file for future use """
	if ACCESS_TOKEN == None or REFRESH_TOKEN ==None:
		sys.exit('access_token or refresh_token missing')
	auth = {'ACCESS_TOKEN': ACCESS_TOKEN, 'REFRESH_TOKEN': REFRESH_TOKEN}
	f = open(AUTH_FILE,'w')
	f.write(json.dumps(auth))
	f.close()


def authenticate_client():
	""" Authenticate the client with the stored access and refresh tokens. """
	try:
		auth = json.load(open(AUTH_FILE))
		ACCESS_TOKEN = auth['ACCESS_TOKEN']
		REFRESH_TOKEN = auth['REFRESH_TOKEN']
		oauth2 = OAuth2(
			client_id=CLIENT_ID,
			client_secret=CLIENT_SECRET,
			access_token=ACCESS_TOKEN,
			refresh_token=REFRESH_TOKEN,
			store_tokens=store_tokens,
			)
		client = Client(oauth2)#, LoggingNetwork())
	except:
		get_authentication_url()
	return client


def get_processed_files():
	file_list = []
	for f in glob.glob('/root/box/files/*/*gz'):
		file_list.append(f.split('/')[-1])
	return file_list

def download_file(folder_item):
	""" Download the file to temp directory. Move it to the proper location when complete to avoid partial files """
	temp_fname = '/tmp/%s' % folder_item.name
	fname = '/root/box/files/unprocessed/%s' % folder_item.name
	f = open(temp_fname,'w')
	folder_item.download_to(f)
	f.close()
	shutil.move(temp_fname,fname)


if __name__ == '__main__':
	file_list = get_processed_files()
	if len(sys.argv) == 2:
		if sys.argv[1] == 'authenticate':
			get_authentication_url()
		else:
			get_initial_tokens(sys.argv[1])
			time.sleep(3)
			sys.exit('you are done, restart the script without the extra token parameter')

	client = authenticate_client()
	session_files = client.search(query='DailySessionLog_RTL*', limit=2000, offset=0, file_extensions=['gz'])
	session_files.sort(key = operator.itemgetter('content_created_at'))
	for session_file in session_files:
		if session_file.name not in file_list:
			print 'downloading %s' % session_file.name ,
			download_file(session_file)
			file_list.append(session_file.name)
			print '... ready'
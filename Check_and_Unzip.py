import glob, gzip, shutil
import sys

for sessionlog in sorted(glob.glob('/root/box/files/unprocessed/DailySessionLog*gz')):
	print 'processing %s'  % sessionlog
	input_file = gzip.open(sessionlog, 'rb')
	try:
		header = input_file.readline() # should be the header
		if header.find('viewerId') < 0: #hmm.. looks like its not.. scroll back and carry on
			print 'NO HEADER??'
			input_file.seek(0)
		tf_location = '/tmp/%s' % sessionlog.replace('/root/box/files/unprocessed/','').replace('.gz','')
		print tf_location
		tf = open(tf_location, 'a')
		 # replace comma-sep with tab to avaid problems with comma in titles
		# for line in input_file:
		# 	tf.write('\t'.join(line.split('\",\"')).replace('"','') )
		tf.write(input_file.read())
	finally:
		tf.close()
		input_file.close()
		shutil.move(tf_location, tf_location.replace('/tmp/', '/mnt/ssddisk/conviva/'))
		shutil.move(sessionlog, sessionlog.replace('/unprocessed/','/processed/'))
		sys.exit()
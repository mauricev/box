COMPLETE THE README!!



--- cron jobs ----
# script which runs every hour and copies gzipped files from box to local unprocessed directory
15 * * * * /usr/bin/python /root/box/Unboxer.py

# script which takes the gzipped file from unprocessed, puts an unzipped version in the inputdirectory for graylog and moves the gzipped file to processed
01 * * * * /usr/bin/python /root/box/Check_and_Unzip.py

# script which runs every day to remove unzipped files from the graylog input directory on the ssddisk
31 1 * * * /usr/bin/python /root/box/Garbageman.py
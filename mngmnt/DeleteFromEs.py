from elasticsearch import Elasticsearch, helpers
import sys, datetime

def delete_by_query(es,query,dryrun=True):
    bulk_deletes = []
    for result in helpers.scan(es, query=query, index="_all"):
    	#      print result['_source']['conviva_session_id'], result['_source']['timestamp'], result['_id']
        result['_op_type'] = 'delete'
        bulk_deletes.append(result)

    print len(bulk_deletes)
    if dryrun==False:
      helpers.bulk(es, bulk_deletes)
    return

es = Elasticsearch()

for offset in range(250):
 filedate = (datetime.date.today() - datetime.timedelta(offset)).strftime('%Y-%m-%d')
 filename = '/mnt/ssddisk/conviva/DailySessionLog_RTL-NL_%s.csv' % filedate
 query = {'query':{'match': {'path': filename}}}
 print query,
 delete_by_query(es=es,query=query,dryrun=True)
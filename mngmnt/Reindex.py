'''from elasticsearch import Elasticsearch, helpers
import sys

def delete_by_query(es,query,dryrun=True):
    bulk_deletes = []
    for result in helpers.scan(es, query=query, index="_all"):
        result['_op_type'] = 'delete'
        bulk_deletes.append(result)

    print len(bulk_deletes)
    if dryrun==False:
      helpers.bulk(es, bulk_deletes)
    return

es = Elasticsearch()

for x in range(1,40):
 filename = '/mnt/ssddisk/conviva/DailySessionLog_RTL-NL_2017-05-%02d.csv' % x
 query = {'query':{'match': {'path': filename}}}
 print query,
 delete_by_query(es=es,query=query,dryrun=False)

filename = '/mnt/ssddisk/conviva/test.csv'
query = {'query':{'match': {'path': filename}}}
print query,
delete_by_query(es=es,query=query,dryrun=False)

'''

from elasticsearch import Elasticsearch
from elasticsearch.helpers import reindex
es = Elasticsearch()

reindex(es, "graylog_0", "graylog_0_new")

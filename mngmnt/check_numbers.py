import requests, json, glob, subprocess

filecounter ={}
for f in sorted(glob.glob('/mnt/ssddisk/conviva/*.csv')):
    linecount = 0
    for line in open(f):
        linecount +=1
    date = f.split('_')[-1].replace('.csv','')
    filecounter[date]= linecount

first = sorted(filecounter)[0]
last = sorted(filecounter)[-1]

print 'found logfiles between: %s - %s' % ( first, last)


url = 'http://localhost:9200/_all/_search?pretty'
headers = {'Content-Type' : 'application/json'}

data = { "size" : 0,
        "aggs": {
            "logfiles": {
                "date_histogram": {
                    "field": "timestamp",
                    "interval": "day",
                    "format": "yyyy-MM-dd",
                    "min_doc_count" : 0,
                    "extended_bounds" : {
                        "min" : first,
                        "max" : last
                        }
                    }
                }
            }
        }

def add_new_file():
    command = '/usr/bin/python /root/box/Check_and_Unzip.py'
    add_file= subprocess.Popen(
            command,
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            close_fds=True,
            shell=True)
    error = None
    return add_file.communicate()

r = requests.get(url, data=json.dumps(data), headers=headers)
#r = requests.get('http://localhost:9200/_all/?pretty', headers=headers)
#print r.json()

logbuckets =  r.json()['aggregations']['logfiles']['buckets']

for logdate in sorted(filecounter):
    found = float([lb['doc_count'] for lb in logbuckets if lb['key_as_string'] == logdate][0])
    missing  = round(((filecounter[logdate] -found)/filecounter[logdate])*100,2)
    if int(missing) < 10:
        ready = True
    else:
        ready = False
    print 'for date: %s the logfile contained %s rows, ES now has %s rows. Diff is %s. Processing is %s' % (logdate, filecounter[logdate], found, missing, ready )
    if logdate == last:
        print 'this is the last file'
        if ready == True:
            print add_new_file()
        else:
            print 'wait'
